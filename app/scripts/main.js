$(document).ready(function() {

  $('#owl-home-reviews').owlCarousel({

    navigation: true,
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false,
    navigationText: [
      '<img src=\'images/icon_owl_left.png\'>',
      '<img src=\'images/icon_owl_right.png\'>'
    ]

  });

  $('#owl-home-wins').owlCarousel({

    navigation: true,
    slideSpeed: 300,
    pagination: false,
    paginationSpeed: 400,
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsDesktopSmall: [768, 1],
    itemsTablet: false,
    itemsMobile: false,
    navigationText: [
      '<img src=\'images/icon_owl_left.png\'>',
      '<img src=\'images/icon_owl_right.png\'>'
    ]

  });

  $('.collapser').mouseenter(function() {
    $(this).next().collapse('toggle');
  });

  /*$('#owl-good').owlCarousel({

    navigation: true,
    slideSpeed: 300,
    pagination: true,
    paginationSpeed: 400,
    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false,
    navigationText: [
      '<img src=\'images/icon_left_small.png\'>',
      '<img src=\'images/icon_right_small.png\'>'
    ]

  });*/

  $('.good-slider').slick({
    nextArrow: '<button class=\'a-right control-c slick-next\'><img src=\'images/icon_right_small.png\'></button>',
    prevArrow: '<button class=\'a-left control-c slick-prev\'><img src=\'images/icon_left_small.png\'></button>',
    dots: true,
    arrows: true
  });

});